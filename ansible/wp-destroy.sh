#!/bin/bash

sudo systemctl stop nginx
sudo systemctl disable nginx
sudo systemctl stop php7.4-fpm
sudo systemctl disable php7.4-fpm

sudo apt-get purge -y nginx nginx-common
sudo apt-get purge -y `dpkg -l | grep php| awk '{print $2}' |tr '\n' ' '`
sudo apt-get autoremove -y

sudo umount /var/www/*.ru/wordpress/wp-content/

sudo rm -Rf /etc/nginx/
sudo rm -Rf /etc/php/
sudo rm -Rf /var/www/*.ru/