#data "vkcs_networking_network" "extnet" {
#   name = "ext-net"
#}

#resource "vkcs_networking_network" "network" {
#   name = "net"
#   admin_state_up = "true"
#}

#resource "vkcs_networking_subnet" "subnetwork" {
#   name       = "subnet"
#   network_id = vkcs_networking_network.network.id
#   cidr       = "10.10.161.0/24"
#   allocation_pool {
#     end   = "10.10.161.99"
#     start = "10.10.161.50"
#  }

#}

#resource "vkcs_networking_router" "router" {
#   name                = "router"
#   admin_state_up      = true
#   external_network_id = data.vkcs_networking_network.extnet.id
#}

#resource "vkcs_networking_router_interface" "router_interface" {
#   router_id = vkcs_networking_router.router.id
#   subnet_id = vkcs_networking_subnet.subnetwork.id
#   depends_on = [
#    vkcs_networking_router.router
#  ]

#}


resource "vkcs_networking_secgroup" "secgroup" {
   name = "security_group"
   description = "terraform security group"
}

resource "vkcs_networking_secgroup_rule" "secgroup_rule_1" {
   direction = "ingress"
   port_range_max = 22
   port_range_min = 22
   protocol = "tcp"
   remote_ip_prefix = "0.0.0.0/0"
   security_group_id = vkcs_networking_secgroup.secgroup.id
   description = "secgroup_rule_1"
}

resource "vkcs_networking_secgroup_rule" "secgroup_rule_2" {
   direction = "ingress"
   port_range_max = 3389
   port_range_min = 3389
   remote_ip_prefix = "0.0.0.0/0"
   protocol = "tcp"
   security_group_id = vkcs_networking_secgroup.secgroup.id
}




resource "vkcs_networking_secgroup_rule" "app_rule_https" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = vkcs_networking_secgroup.app.id
}

resource "vkcs_networking_secgroup" "app" {
  name        = "app_fw"
  description = "apps"
}

resource "vkcs_networking_secgroup_rule" "app_rule_http" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = vkcs_networking_secgroup.app.id
}

resource "vkcs_networking_secgroup_rule" "app_rule_telegraf" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 9125
  port_range_max    = 9125
  remote_ip_prefix  = "10.10.161.0/24"
  security_group_id = vkcs_networking_secgroup.app.id
}

resource "vkcs_networking_secgroup_rule" "app_rule_nginx_exporter" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 9113
  port_range_max    = 9113
  remote_ip_prefix  = "10.10.161.0/24"
  security_group_id = vkcs_networking_secgroup.app.id
}
resource "vkcs_networking_secgroup_rule" "app_rule_node_exporter" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 9100
  port_range_max    = 9100
  remote_ip_prefix  = "10.10.161.0/24"
  security_group_id = vkcs_networking_secgroup.app.id
}

resource "vkcs_networking_secgroup_rule" "app_rule_ssh" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "10.10.161.0/24"
  security_group_id = vkcs_networking_secgroup.app.id
}




resource "vkcs_networking_secgroup" "mon" {
  name        = "mon_fw"
  description = "monitoring"
}
resource "vkcs_networking_secgroup_rule" "prometheus" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 9090
  port_range_max    = 9090
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = vkcs_networking_secgroup.mon.id
}
resource "vkcs_networking_secgroup_rule" "alertmanager" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 9093
  port_range_max    = 9093
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = vkcs_networking_secgroup.mon.id
}
resource "vkcs_networking_secgroup_rule" "prom_ssh" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = vkcs_networking_secgroup.mon.id
}
resource "vkcs_networking_secgroup_rule" "grafana" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = vkcs_networking_secgroup.mon.id
}
resource "vkcs_networking_secgroup_rule" "prom_node_exporter" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 9100
  port_range_max    = 9100
  remote_ip_prefix  = "10.10.161.0/24"
  security_group_id = vkcs_networking_secgroup.mon.id
}
