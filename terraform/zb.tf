
variable "db-instance-flavor" {
  type    = string
  default = "Standard-2-8-50"
}

data "vkcs_networking_network" "extnet" {
   name = "ext-net"
}

resource "vkcs_networking_network" "network" {
   name = "net"
   admin_state_up = "true"
}

resource "vkcs_networking_subnet" "subnetwork" {
   name       = "subnet"
   network_id = vkcs_networking_network.network.id
   cidr       = "10.10.161.0/24"
   dns_nameservers = ["8.8.8.8", "8.8.4.4"]
   allocation_pool {
     end   = "10.10.161.99"
     start = "10.10.161.50"
  }

}

resource "vkcs_networking_router" "router" {
   name                = "router"
   admin_state_up      = true
   external_network_id = data.vkcs_networking_network.extnet.id
}

resource "vkcs_networking_router_interface" "router_interface" {
   router_id = vkcs_networking_router.router.id
   subnet_id = vkcs_networking_subnet.subnetwork.id
}



resource "vkcs_db_instance" "db01" {
  name              = "db01"
  keypair           = var.keypair_name
  flavor_id         = data.vkcs_compute_flavor.db01.id
  availability_zone = "GZ1"
  size              = 8
  volume_type       = "ceph-ssd"

  datastore {
    type    = "mysql"
    version = "8.0"
  }

  disk_autoexpand {
    autoexpand    = true
    max_disk_size = 1000
  }

  network {
    uuid = vkcs_networking_network.network.id
#     uuid ="${vkcs_networking_network.network.id}"

  }

  capabilities {
    name = "node_exporter"
    settings = {
      "listen_port" : "9100"
    }
  }

  capabilities {
    name = "mysqld_exporter"
    settings = {
      "listen_port" : "9104"
    }
  }
  depends_on = [
    vkcs_networking_subnet.subnetwork,
    vkcs_networking_router_interface.router_interface
  ]
}


resource "vkcs_db_database" "wp-database" {
  name    = "wpdb"
  dbms_id = vkcs_db_instance.db01.id
  charset = "utf8"
  collate = "utf8_general_ci"
}


resource "vkcs_db_user" "db-user" {
  name        = "testuser"
  password    = var.db_user_password
  dbms_id = vkcs_db_instance.db01.id
  databases   = [vkcs_db_database.wp-database.name]
}
