data "vkcs_compute_flavor" "prom" {
  name = var.compute_flavor
}

data "vkcs_images_image" "prom" {
  name = var.image_flavor
}



resource "vkcs_compute_instance" "prom" {
  name      = "prom1"
  flavor_id = data.vkcs_compute_flavor.prom.id
  security_groups = [vkcs_networking_secgroup.mon.name]
  image_name        = data.vkcs_images_image.prom.id
  availability_zone       = "MS1"
  key_pair          = var.keypair_name
  config_drive      = true
 user_data = templatefile("../script/mon.sh", {
    APP01_ADDRESS = vkcs_compute_instance.compute_1.access_ip_v4,
    APP02_ADDRESS = vkcs_compute_instance.compute_2.access_ip_v4,
    DB01_ADDRESS  = vkcs_db_instance.db01.ip[0],
    BOT_CHAT      = var.bot_chat,
    BOT_TOKEN     = var.bot_token

})
  block_device {
    uuid                  = data.vkcs_images_image.prom.id
    source_type           = "image"
    destination_type      = "volume"
    volume_type           = "ceph-ssd"
    volume_size           = 8
    boot_index            = 0
    delete_on_termination = true
  }

  network {
    uuid        = vkcs_networking_network.network.id
    fixed_ip_v4 = "10.10.161.210"
  }
  depends_on = [
    vkcs_networking_router_interface.router_interface
  ]
}

resource "vkcs_networking_floatingip" "prom_fip" {
  pool = data.vkcs_networking_network.extnet.name
}

resource "vkcs_compute_floatingip_associate" "mon_fip" {
  floating_ip = vkcs_networking_floatingip.prom_fip.address
  instance_id = vkcs_compute_instance.prom.id
}
