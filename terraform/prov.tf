terraform {
  backend "s3" {
    bucket                      = "word"
    region                      = "us-east-1"
    key                         = "terraform.tfstate"
    endpoint                    = "hb.ru-msk.vkcs.cloud"
    skip_credentials_validation = true
  }
  required_providers {
    vkcs = {
      source  = "vk-cs/vkcs"
    }
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.48.0"
    }
  }
}

provider "vkcs" {
    username = "nether88@gmail.com"
    password = "HC33ePE9"
    project_id = "d812c24528a1430e97ad706733a2f174" 
    region = "RegionOne"
    auth_url = "https://infra.mail.ru:35357/v3/" 
}


resource "vkcs_compute_keypair" "keypair" {
  name = var.keypair_name
}

resource "local_file" "keypair" {         
content         = <<-EOL
    ${vkcs_compute_keypair.keypair.private_key}
    EOL
  filename        = "${path.module}/privatekey.pem"
  file_permission = 400
}


resource "local_file" "address" {
    content  = vkcs_networking_floatingip.prom_fip.address
    filename = "${path.module}/ip"
}


resource "local_file" "share" {
    content  = vkcs_sharedfilesystem_share.share1.export_location_path
    filename = "${path.module}/share"
}
output "share" {
  description = "share"
  value       = vkcs_db_instance.db01.ip[0]
  sensitive = true
}

resource "local_file" "db" {
    content  = vkcs_db_instance.db01.ip[0]
    filename = "${path.module}/db"
}
