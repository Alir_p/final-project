data "vkcs_images_image" "compute" {
   name = "Ubuntu-20.04.1-202008"
}

data "vkcs_compute_flavor" "compute" {
  name = "Basic-1-2-20"
}

resource "vkcs_compute_instance" "compute_1" {
  name            = "compute-instance-1"
  flavor_id       = data.vkcs_compute_flavor.compute.id
  key_pair        = var.keypair_name
  security_groups = [vkcs_networking_secgroup.app.name]
  image_id = data.vkcs_images_image.compute.id
  user_data = templatefile("../script/wp.sh" , {})

  network {
    uuid = vkcs_networking_network.network.id
    fixed_ip_v4 = "10.10.161.110"
  }

  depends_on = [
    vkcs_networking_network.network,
    vkcs_networking_subnet.subnetwork,
    vkcs_networking_router_interface.router_interface
  ]
}

resource "vkcs_compute_instance" "compute_2" {
  name            = "compute-instance-2"
  flavor_id       = data.vkcs_compute_flavor.compute.id
  key_pair        = var.keypair_name
  user_data = templatefile("../script/wp.sh" , {})
  security_groups = [vkcs_networking_secgroup.app.name]
  image_id = data.vkcs_images_image.compute.id

  network {
    uuid = vkcs_networking_network.network.id
    fixed_ip_v4 = "10.10.161.111"
  }

  depends_on = [
    vkcs_networking_network.network,
    vkcs_networking_subnet.subnetwork,
    vkcs_networking_router_interface.router_interface
  ]
}
resource "vkcs_networking_floatingip" "lb_fip" {
  pool = data.vkcs_networking_network.extnet.name
}


resource "vkcs_lb_loadbalancer" "loadbalancer" {
  name = "loadbalancer"
  vip_subnet_id = "${vkcs_networking_subnet.subnetwork.id}"
  depends_on = [
  vkcs_compute_instance.compute_1,
  vkcs_compute_instance.compute_2
 ]

}

resource "vkcs_lb_listener" "listener" {
  name = "listener"
  protocol = "HTTP"
  protocol_port = 80
  loadbalancer_id = "${vkcs_lb_loadbalancer.loadbalancer.id}"
}

resource "vkcs_lb_pool" "pool" {
  name = "pool"
  protocol = "HTTP"
  lb_method = "ROUND_ROBIN"
  listener_id = "${vkcs_lb_listener.listener.id}"
}

resource "vkcs_lb_member" "member_1" {
  address = "10.10.161.110"
  protocol_port = 80
  pool_id = "${vkcs_lb_pool.pool.id}"
  subnet_id = "${vkcs_networking_subnet.subnetwork.id}"
  weight = 1
}

resource "vkcs_lb_member" "member_2" {
  address = "10.10.161.111"
  protocol_port = 80
  pool_id = "${vkcs_lb_pool.pool.id}"
  subnet_id = "${vkcs_networking_subnet.subnetwork.id}"
  weight = 1
}


resource "vkcs_networking_floatingip_associate" "lb_fip" {
  floating_ip = vkcs_networking_floatingip.lb_fip.address
  port_id     = vkcs_lb_loadbalancer.loadbalancer.vip_port_id
}
