variable "image_flavor" {
  type = string
  default = "Ubuntu-22.04-202208"
}

variable "compute_flavor" {
  type = string
  default = "Basic-1-2-20"
}


variable "keypair_name" {
  default = "keypair"
}

variable "cloudflare_api_token" {
  default = ""
}

variable "cloudflare_account_id" {
  default = ""
}

variable "bot_chat" {
  default = ""
}

variable "bot_token" {
  default = ""
}

#variable "db-instance-flavor" {
#  type    = string
#  default = "Standard-2-8-50"
#}

data "vkcs_compute_flavor" "db01" {
  name = var.db-instance-flavor
}

variable "db_user_password" {
  type      = string
  sensitive = true
}

