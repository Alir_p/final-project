#!/bin/bash


sudo echo '
${APP01_ADDRESS} comp1
${APP02_ADDRESS} comp2
${DB01_ADDRESS} db01
localhost mon
' >> /etc/hosts


sudo apt-get update
sudo apt-get install -y wget gnupg2

sudo adduser --no-create-home --disabled-login --shell /bin/false --gecos "Prometheus Monitoring User" prometheus
sudo mkdir /etc/prometheus
sudo mkdir /var/lib/prometheus
sudo touch /etc/prometheus/prometheus.yml
sudo touch /etc/prometheus/prometheus.rules.yml
sudo chown -R prometheus:prometheus /etc/prometheus
sudo chown prometheus:prometheus /var/lib/prometheus
wget https://github.com/prometheus/prometheus/releases/download/v2.43.0-rc.0/prometheus-2.43.0-rc.0.linux-amd64.tar.gz
tar xvzf prometheus-2.43.0-rc.0.linux-amd64.tar.gz
sudo cp prometheus-2.43.0-rc.0.linux-amd64/prometheus /usr/local/bin/
sudo cp prometheus-2.43.0-rc.0.linux-amd64/promtool /usr/local/bin/
sudo cp -r prometheus-2.43.0-rc.0.linux-amd64/consoles /etc/prometheus
sudo cp -r prometheus-2.43.0-rc.0.linux-amd64/console_libraries /etc/prometheus
sudo chown -R prometheus:prometheus /etc/prometheus/consoles
sudo chown -R prometheus:prometheus /etc/prometheus/console_libraries
sudo chown prometheus:prometheus /usr/local/bin/prometheus
sudo chown prometheus:prometheus /usr/local/bin/promtool
sudo echo '
global:
  scrape_interval: 15s

rule_files:
  - 'prometheus.rules.yml'

scrape_configs:

  - job_name: 'prometheus'
    scrape_interval: 5s
    static_configs:
      - targets: ['localhost:9090']

  - job_name: 'node_exporter'
    scrape_interval: 5s
    static_configs:
      - targets: ['localhost:9100']
      - targets: ['comp1:9100']
      - targets: ['comp2:9100']
      - targets: ['db01:9100']

  - job_name: 'nginx exporter'
    scrape_interval: 5s
    static_configs:
      - targets: ['comp1:9113']
      - targets: ['comp2:9113']

  - job_name: 'mysqld_exporter'
    scrape_interval: 5s
    static_configs:
      - targets: ['db01:9104']

  - job_name: 'grafana'
    scrape_interval: 5s
    static_configs:
      - targets:
        - localhost:3000

  - job_name: 'telegraf'
    scrape_interval: 5s
    static_configs:
      - targets: ['comp1:9125']
      - targets: ['comp2:9125']

alerting:
  alertmanagers:
  - static_configs:
    - targets:
      - localhost:9093
' > /etc/prometheus/prometheus.yml

sudo echo '
groups:
  - name: vm
    rules:
      - alert: InstanceDown
        expr: up == 0
        for: 1m
        labels:
          severity: page
        annotations:
          summary: "Instance {{ $labels.instance }} down"
          description: "{{ $labels.instance }} of job {{ $labels.job }} has been down for more than 1 minute."

  - name: disk
    rules:
      - alert: DiskLatencyHigh
        expr: node_disk_latency_seconds{device=~"sd\\d+"} > 0.1
        labels:
          severity: warning
        annotations:
          summary: "Disk latency is high (instance {{ $labels.instance }})"
          description: "Disk latency is high (value: {{ $value }})."

  - name: memory
    rules:
      - alert: HighMemoryUsage
        expr: (node_memory_Active_bytes / node_memory_MemTotal_bytes) * 100 > 80
        for: 5m
        labels:
          severity: warning
        annotations:
          summary: "High memory usage on {{ $labels.instance }}"
          description: "{{ $labels.instance }} has more than 80% memory usage (value: {{ humanize $value }})."

  - name: space
    rules:
      - alert: LowDiskSpace
        expr: node_filesystem_free_bytes{mountpoint="/"}/1024/1024 < 2048
        labels:
          severity: warning
        annotations:
          summary: "Low disk space on {{ $labels.mountpoint }} in {{ $labels.instance }}"
          description: "{{ $labels.mountpoint }} has less than 2GB of free space (value: {{ $value }})."
' > /etc/prometheus/prometheus.rules.yml



sudo echo '
[Unit]
Description=Prometheus
Wants=network-online.target
After=network-online.target

[Service]
User=prometheus
Group=prometheus
Type=simple
ExecStart=/usr/local/bin/prometheus \
    --config.file /etc/prometheus/prometheus.yml \
    --storage.tsdb.path /var/lib/prometheus/ \
    --web.console.templates=/etc/prometheus/consoles \
    --web.console.libraries=/etc/prometheus/console_libraries

[Install]
WantedBy=multi-user.target
' > /etc/systemd/system/prometheus.service
sudo systemctl daemon-reload
sudo systemctl enable prometheus
sudo systemctl start prometheus
rm prometheus-2.43.0-rc.0.linux-amd64.tar.gz
rm -rf prometheus-2.43.0-rc.0.linux-amd64

sudo apt-get install -y adduser libfontconfig1
wget https://mirrors.huaweicloud.com/grafana/9.4.3/grafana-enterprise_9.4.3_amd64.deb
sudo dpkg -i grafana-enterprise_9.4.3_amd64.deb
sudo iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 3000
sudo echo iptables-persistent iptables-persistent/autosave_v4 boolean true | sudo debconf-set-selections
sudo echo iptables-persistent iptables-persistent/autosave_v6 boolean true | sudo debconf-set-selections
sudo apt install iptables-persistent -y
sudo echo '

apiVersion: 1

datasources:
  - name: Prometheus
    type: prometheus
    access: proxy
    orgId: 1
    url: http://localhost:9090
    basicAuth: false
    isDefault: false
    jsonData:
      timeInterval: "5s"
      httpMethod: "GET"
      tlsSkipVerify: false
      version: 1
' > /etc/grafana/provisioning/datasources/prometheus-gb.yml
sudo mkdir -p /var/lib/grafana/dashboards
wget -O /var/lib/grafana/dashboards/node-exporter.json https://grafana.com/api/dashboards/1860/revisions/30/download
wget -O /var/lib/grafana/dashboards/mysql-exporter.json https://grafana.com/api/dashboards/14057/revisions/1/download
wget -O /var/lib/grafana/dashboards/telegraf.json https://grafana.com/api/dashboards/16388/revisions/2/download
sudo echo '
apiVersion: 1
providers:
- name: 'node-exporter'
  orgId: 1
  folder: ''
  type: file
  disableDeletion: false
  updateIntervalSeconds: 10
  options:
    path: /var/lib/grafana/dashboards
    foldersFromFilesStructure: true
' > /etc/grafana/provisioning/dashboards/node-exporter.yml
sudo systemctl daemon-reload
sudo systemctl enable grafana-server
sudo systemctl start grafana-server
rm grafana-enterprise_9.4.3_amd64.deb

sudo adduser --no-create-home --disabled-login --shell /bin/false --gecos "Alertmanager User" alertmanager
sudo mkdir /etc/alertmanager
sudo mkdir /etc/alertmanager/template
sudo mkdir -p /var/lib/alertmanager/data
sudo touch /etc/alertmanager/alertmanager.yml
sudo chown -R alertmanager:alertmanager /etc/alertmanager
sudo chown -R alertmanager:alertmanager /var/lib/alertmanager
wget https://github.com/prometheus/alertmanager/releases/download/v0.25.0/alertmanager-0.25.0.linux-amd64.tar.gz 
tar xvzf alertmanager-0.25.0.linux-amd64.tar.gz
sudo cp alertmanager-0.25.0.linux-amd64/alertmanager /usr/local/bin/
sudo cp alertmanager-0.25.0.linux-amd64/amtool /usr/local/bin/
sudo chown alertmanager:alertmanager /usr/local/bin/alertmanager
sudo chown alertmanager:alertmanager /usr/local/bin/amtool
sudo echo '
global:

# catch-all route to receive and handle all incoming alerts
route:
  group_by: ['alertname']
  group_wait: 10s       # wait up to 10s for more alerts to group them
  receiver: 'telepush'  # see below

# telepush configuration here
receivers:
- name: 'telepush'
  webhook_configs:
  - url: 'https://telepush.dev/api/inlets/alertmanager/9e12a5'    # add your Telepush token here
    http_config:
' > /etc/alertmanager/alertmanager.yml

sudo echo '
[Unit]
Description=Prometheus Alert Manager service
Wants=network-online.target
After=network.target

[Service]
User=alertmanager
Group=alertmanager
Type=simple
ExecStart=/usr/local/bin/alertmanager \
    --config.file /etc/alertmanager/alertmanager.yml \
    --storage.path /var/lib/alertmanager/data
Restart=always

[Install]
WantedBy=multi-user.target
' > /etc/systemd/system/alertmanager.service
sudo systemctl daemon-reload
sudo systemctl enable alertmanager
sudo systemctl start alertmanager
rm alertmanager-0.25.0.linux-amd64.tar.gz
rm -rf alertmanager-0.25.0.linux-amd64

sudo adduser --no-create-home --disabled-login --shell /bin/false --gecos "Node Exporter User" node_exporter
wget https://github.com/prometheus/node_exporter/releases/download/v1.5.0/node_exporter-1.5.0.linux-amd64.tar.gz
tar xvzf node_exporter-1.5.0.linux-amd64.tar.gz
sudo cp node_exporter-1.5.0.linux-amd64/node_exporter /usr/local/bin/
sudo chown node_exporter:node_exporter /usr/local/bin/node_exporter
sudo echo '
[Unit]
Description=Node Exporter
Wants=network-online.target
After=network-online.target

[Service]
User=node_exporter
Group=node_exporter
Type=simple
ExecStart=/usr/local/bin/node_exporter

[Install]
WantedBy=multi-user.target
' > /etc/systemd/system/node_exporter.service
sudo systemctl daemon-reload
sudo systemctl enable node_exporter
sudo systemctl start node_exporter
rm node_exporter-1.5.0.linux-amd64.tar.gz
rm -rf node_exporter-1.5.0.linux-amd64s
