#!/bin/bash

sudo adduser --no-create-home --disabled-login --shell /bin/false --gecos "Node Exporter User" node_exporter
wget https://github.com/prometheus/node_exporter/releases/download/v1.5.0/node_exporter-1.5.0.linux-amd64.tar.gz
tar xvzf node_exporter-1.5.0.linux-amd64.tar.gz
sudo cp node_exporter-1.5.0.linux-amd64/node_exporter /usr/local/bin/
sudo chown node_exporter:node_exporter /usr/local/bin/node_exporter
sudo echo '
[Unit]
Description=Node Exporter
Wants=network-online.target
After=network-online.target

[Service]
User=node_exporter
Group=node_exporter
Type=simple
ExecStart=/usr/local/bin/node_exporter

[Install]
WantedBy=multi-user.target
' > /etc/systemd/system/node_exporter.service

sudo systemctl daemon-reload
sudo systemctl enable node_exporter
sudo systemctl start node_exporter
rm node_exporter-1.5.0.linux-amd64.tar.gz
rm -rf node_exporter-1.5.0.linux-amd64

wget https://github.com/nginxinc/nginx-prometheus-exporter/releases/download/v0.11.0/nginx-prometheus-exporter_0.11.0_linux_amd64.tar.gz
tar -zxf nginx-prometheus-exporter_0.11.0_linux_amd64.tar.gz
sudo adduser --no-create-home --disabled-login --shell /bin/false --gecos "NginX Exporter User" nginx_exporter
sudo cp nginx-prometheus-exporter /usr/local/bin/
sudo chown nginx_exporter:nginx_exporter /usr/local/bin/nginx-prometheus-exporter
sudo echo '
[Unit]
Description=NGINX Prometheus Exporter
After=network.target

[Service]
Type=simple
User=nginx_exporter
Group=nginx_exporter
ExecStart=/usr/local/bin/nginx-prometheus-exporter \
    -web.listen-address=0.0.0.0:9113 \
    -nginx.scrape-uri http://127.0.0.1:81/metrics

SyslogIdentifier=nginx_prometheus_exporter
Restart=always

[Install]
WantedBy=multi-user.target
' > /etc/systemd/system/nginx-exporter.service
sudo systemctl enable nginx-exporter
sudo systemctl start nginx-exporter
sudo rm nginx-prometheus-exporter_0.11.0_linux_amd64.tar.gz
sudo chmod 755 /var/log/nginx/access.log /var/log/nginx/error.log
wget -q https://repos.influxdata.com/influxdata-archive_compat.key
sudo echo '393e8779c89ac8d958f81f942f9ad7fb82a25e133faddaf92e15b16e6ac9ce4c influxdata-archive_compat.key' | sha256sum -c && cat influxdata-archive_compat.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/influxdata-archive_compat.gpg > /dev/null
sudo echo 'deb [signed-by=/etc/apt/trusted.gpg.d/influxdata-archive_compat.gpg] https://repos.influxdata.com/debian stable main' | sudo tee /etc/apt/sources.list.d/influxdata.list
sudo apt-get update && sudo apt-get install telegraf

sudo echo '
#nginx-metrics and logs
[[inputs.nginx]]
    urls = ["http://localhost/metrics"]
    response_timeout = "5s"
[[inputs.tail]]
    name_override = "nginxlog"
    files = ["/var/log/nginx/access.log"]
    from_beginning = true
    pipe = false
    data_format = "grok"
    grok_patterns = ["%%{COMBINED_LOG_FORMAT}"]
' > /etc/telegraf/telegraf.conf

sudo echo '
#Output plugin
[[outputs.prometheus_client]]
    listen = "0.0.0.0:9125"
' >> /etc/telegraf/telegraf.conf

sudo systemctl restart telegraf
